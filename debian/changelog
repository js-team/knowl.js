knowl.js (0~20190101-2) unstable; urgency=medium

  * debian/clean
    - Clean up minified css file during dh_clean.
  * debian/control
    - Bump Standards-Version to 4.6.0.
    - Move libjs-jquery from Depends to Recommends; it's possible to use
      knowls without the jQuery package, e.g., by including it via a CDN.
    - Add cleancss to Build-Depends.
  * debian/install
    - Install minified css file.
  * debian/rules
    - Generate minified css file during build.
    - Simplify get-orig-source target.

 -- Doug Torrance <dtorrance@piedmont.edu>  Fri, 27 Aug 2021 20:22:00 -0400

knowl.js (0~20190101-1) unstable; urgency=medium

  * New upstream release.

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure URI in Homepage field.
  * Change priority extra to priority optional.
  * Apply multi-arch hints.
    + libjs-knowl: Add Multi-Arch: foreign.

  [ Doug Torrance ]
  * debian/control
    - Bump debhelper compatibility level to 13.
    - Bump Standards-Version to 4.5.1.
    - Update Vcs-* fields to point to Salsa.
    - Add Rules-Requires-Root field; set to "no".
    - Fix Section (web -> javascript).
    - Add libjs-jquery to Depends.
    - Add libjs-mathjax to Recommends; knowls work without it, but we
      get errors.  (We also get errors about missing sagecell, but that
      isn't in Debian.)
  * debian/rules
    - Use https for download link in get-orig-source target.
    - Use -v option to mv in get-orig-source so that tarball filename is
      displayed.
  * debian/watch
    - Use fake upstream since there is no release tarball.

 -- Doug Torrance <dtorrance@piedmont.edu>  Mon, 16 Aug 2021 21:04:07 -0400

knowl.js (0~20160130-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 12:28:32 +0100

knowl.js (0~20160130-1) unstable; urgency=low

  * Initial release (Closes: #813852).

 -- Doug Torrance <dtorrance@piedmont.edu>  Fri, 05 Feb 2016 17:19:08 -0500
